var counter_switch = 0;
var animated = 0;

$(document).ready(function(){

	scrollTop();
	sliderInit();
	setTimeout(function(){
		AOS.init();
	},1000);

	AOS.init();
	bannerResize();


	$('.slider').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		dots:false,		
        arrows: true,	
  		focusOnSelect: false,
		responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	      }
	    },
	    {
	      breakpoint: 767,
	      settings: {
	        slidesToShow: 2,        
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	      }
	    }
	  ]
	});


	$('.back-to-top').on('click',function(){   
		$('html,body').stop().animate({
			scrollTop: '0px',
		});
	});

	$('.menu-btn').on('click',function(e){
		e.preventDefault();
		$('nav').slideToggle();
		$(this).children('i').toggleClass('fa-times');
	});

	$('nav .bottom-nav a').on('click',function(){
		if($(window).width() < 991){
			$('nav').slideUp();			
			$('.menu-btn i').removeClass('fa-times');
		}
	});


	$('.multiselect').each(function(){

		var placeholder = (typeof($(this).data('multiselect-placeholder')) != 'undefined')?$(this).data('multiselect-placeholder'):'';
		var btnimage = (typeof($(this).data('multiselect-btnimage')) != 'undefined')?'<img src="'+$(this).data('multiselect-btnimage')+'"/>':'';




		$(this).multiselect({
		    columns: 1,
		    placeholder: btnimage+placeholder,
		});
	});
});

$(window).resize(function(){
	bannerResize();
	scrollTop();
});

$(window).scroll(function() {
	scrollTop();
});



function scrollTop(){
	if($(window).scrollTop() > 700){		
		$('.back-to-top').fadeIn();
	}
	else{
		$('.back-to-top').fadeOut();
	}


	if($(window).scrollTop() > 200){		
		$('header').addClass('fixed');
	}
	else{
		$('header').removeClass('fixed');
	}	
}

function hashScroll(){
    var hash = window.location.hash ;
    if(hash != ''){
		var padding = $('header').outerHeight()+130;
		$(hash).animatescroll({scrollSpeed:100,hash,padding:padding});
		window.location.hash = hash;
	}    
}

function bannerResize(){
	var banner_height = $(window).height() - $('header').outerHeight();
	if(banner_height > 0){
		$('.banner').css('height',banner_height+'px');
	}
	else{		
		$('.banner').css('height','700px');
	}
}



function counter(){
	if(animated == 0){
		(function ($) {
			$.fn.countTo = function (options) {
				options = options || {};
				
				return $(this).each(function () {
					// set options for current element
					var settings = $.extend({}, $.fn.countTo.defaults, {
						from:            $(this).data('from'),
						to:              $(this).data('to'),
						append:          ($(this).data('append') != undefined)?$(this).data('append'):'',
						speed:           $(this).data('speed'),
						refreshInterval: $(this).data('refresh-interval'),
						decimals:        $(this).data('decimals')
					}, options);
					
					// how many times to update the value, and how much to increment the value on each update
					var loops = Math.ceil(settings.speed / settings.refreshInterval),
						increment = (settings.to - settings.from) / loops;
					
					// references & variables that will change with each update
					var self = this,
						$self = $(this),
						loopCount = 0,
						value = settings.from,
						data = $self.data('countTo') || {};
					
					$self.data('countTo', data);
					
					// if an existing interval can be found, clear it first
					if (data.interval) {
						clearInterval(data.interval);
					}
					data.interval = setInterval(updateTimer, settings.refreshInterval);
					
					// initialize the element with the starting value
					render(value);
					
					function updateTimer() {
						value += increment;
						loopCount++;
						
						render(value);
						
						if (typeof(settings.onUpdate) == 'function') {
							settings.onUpdate.call(self, value);
						}
						
						if (loopCount >= loops) {
							// remove the interval
							$self.removeData('countTo');
							clearInterval(data.interval);
							value = settings.to;
							
							if (typeof(settings.onComplete) == 'function') {
								settings.onComplete.call(self, value);
							}
						}
					}
					
					function render(value) {
						var formattedValue = settings.formatter.call(self, value, settings);
						$self.html(formattedValue+settings.append);
					}
				});
			};
			
			$.fn.countTo.defaults = {
				from: 0,               // the number the element should start at
				to: 0,                 // the number the element should end at
				speed: 1000,           // how long it should take to count between the target numbers
				refreshInterval: 100,  // how often the element should be updated
				decimals: 0,           // the number of decimal places to show
				formatter: formatter,  // handler for formatting the value before rendering
				onUpdate: null,        // callback method for every time the element is updated
				onComplete: null       // callback method for when the element finishes updating
			};
			
			function formatter(value, settings) {
				return value.toFixed(settings.decimals);
			}
		}(jQuery));

		jQuery(function ($) {
		  // custom formatting example
		  $('.count-number').data('countToOptions', {
			formatter: function (value, options) {
			  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
			}
		  });
		  
		  // start all the timers
		  $('.timer').each(count);  
		  
		  function count(options) {
			var $this = $(this);
			options = $.extend({}, options || {}, $this.data('countToOptions') || {});
			$this.countTo(options);
		  }
		});	

        animated = 1;
	}
}


$('.dots-col .dots').on('click','.dot',function(e){
	e.preventDefault();
	var slide_num = $(this).data('slide-num');
	slider(slide_num);
});

function slider(index){
	$('.banner-slider-track .slide').hide();
	$('.banner-slider-track .slide').each(function(i){
		if(i == index){
			var banner_heading = $(this).data('slide-heading');
			var banner_sub_heading = $(this).data('slide-sub-heading');	
			$('.banner-slider-content .heading').text(banner_heading);
			$('.banner-slider-content .sub-heading').text(banner_sub_heading);
			$(this).fadeIn('slow');
		}
	});

	$('.dots-col .dots .dot').each(function(i){
		$(this).removeClass('active');
		if(i == index){
			$(this).addClass('active');
		}
	});	
}

function sliderInit(){
	$('.banner-slider-track .slide').each(function(i){
		var banner_heading = $(this).data('slide-heading');
		var banner_sub_heading = $(this).data('slide-sub-heading');
		var html = '<li><a data-slide-num="'+i+'" href="#" class="dot"></a></li>';
		if(i == 0){
			$('.banner-slider-content .heading').text(banner_heading);
			$('.banner-slider-content .sub-heading').text(banner_sub_heading);
			html = '<li><a data-slide-num="'+i+'" href="#" class="dot active"></a></li>';
		}
		$('.dots-col .dots').append(html);
	});
}